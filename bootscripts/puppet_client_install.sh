#!/bin/bash

if [[ `/usr/bin/lsb_release -si` == "Ubuntu" ]];then
  echo "104.47.162.100 puppet" >> /etc/hosts
  cd /tmp
  /usr/bin/wget https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
  /usr/bin/dpkg -i puppetlabs-release-pc1-xenial.deb
  /usr/bin/apt update
  /usr/bin/apt-get install puppet-agent
else
  yum install -y https://pm.puppetlabs.com/puppet-agent/2017.2.2/1.10.4/repos/el/7/PC1/x86_64/puppet-agent-1.10.4-1.el7.x86_64.rpm
  echo "10.200.20.12 puppet" >> /etc/hosts
fi
/opt/puppetlabs/bin/puppet agent --server puppet --waitforcert 60 --test