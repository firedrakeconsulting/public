class dokuwiki::httpd {
  package { 'httpd':
    ensure => installed,
  }
}
